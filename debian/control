Source: ruby-sshkey
Section: ruby
Priority: optional
Maintainer: Debian Ruby Team <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               rake
Standards-Version: 4.5.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-sshkey.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-sshkey
Homepage: https://github.com/bensie/sshkey
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all
Rules-Requires-Root: no

Package: ruby-sshkey
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby:any | ruby-interpreter,
         ${misc:Depends}
Breaks: gitlab (<< 11.9~)
Description: SSH private/public key generator in Ruby
 Generate private/public SSH keypairs (RSA and DSA supported) using pure Ruby.
 .
 When generating a new keypair the default key type is 2048-bit RSA, but you
 can supply the type (RSA or DSA) and bits in the options. You can also
 (optionally) supply a comment or passphrase.
 .
 This library can read a public or private key and present it as a string.
